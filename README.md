# Flectra Community / survey

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[survey_conditional_question](survey_conditional_question/) | 2.0.1.0.0| Survey Conditional Questions
[survey_question_type_nps](survey_question_type_nps/) | 2.0.1.0.0|         This module add nps rating as question type for survey page
[survey_xlsx](survey_xlsx/) | 2.0.1.0.0|         XLSX Report to show the survey results
[survey_question_type_five_star](survey_question_type_five_star/) | 2.0.1.0.1|         This module add five stars rating as question type for survey page
[survey_description](survey_description/) | 2.0.1.0.0|                Displays description and thank you fields for survey,               page and question.               


